// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBHNbeRFxO1svVcu3PmYrKhNq5oZH-9B3E",
    authDomain: "exemple2-4f493.firebaseapp.com",
    projectId: "exemple2-4f493",
    storageBucket: "exemple2-4f493.appspot.com",
    messagingSenderId: "556015970249",
    appId: "1:556015970249:web:28c03d9185391244346a18"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
