import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardComponent } from './card/card.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { CustomersComponent } from './customers/customers.component';
import { WelocomeComponent } from './welocome/welocome.component';
import { PostsComponent } from './posts/posts.component';
import { CommentsComponent } from './comments/comments.component';
import { SavepostComponent } from './savepost/savepost.component';

const routes: Routes = [
  { path: 'cards', component: CardComponent },
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'customers', component: CustomersComponent},
  { path: 'welcome', component: WelocomeComponent},
  { path: 'posts', component: PostsComponent},
  { path: 'comments', component: CommentsComponent},
  { path: 'savepost', component: SavepostComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
