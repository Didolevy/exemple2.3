import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class SavepostService {

  savepostsCollection:AngularFirestoreCollection;  
  public deleteSave(userId:string , id:string){
    this.db.doc(`users/${userId}/saveposts/${id}`).delete();
  }

  public getSave(userId){
    this.savepostsCollection = this.db.collection(`users/${userId}/saveposts`);
    return this.savepostsCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }


  constructor(private db:AngularFirestore) { }
}
