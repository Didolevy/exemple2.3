import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Comment} from './interfaces/comment';


@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  private URL = "https://jsonplaceholder.typicode.com/comments";

  constructor(private http: HttpClient) { }

  getComments():Observable<Comment>{

    return this.http.get<Comment>(this.URL); 
  }

}
