import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';



@Component({
  selector: 'cards',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  cards= [{name:"Adi",age:"26",role:"Analyst",discreption:"Greate Worker"}];
  constructor(public authService:AuthService) { }

  ngOnInit(): void {
  }

}
