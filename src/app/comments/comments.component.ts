import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommentsService } from '../comments.service';


@Component({
  selector: 'comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  comments$;
  postId: number;
  name:string;
  email:string;
  body:string;
  panelOpenState = false;
  hasError = false;
  @Input() id: number;
   
  constructor(private route:ActivatedRoute ,private costsService:CommentsService) { }

  

  ngOnInit(): void {
    this.comments$=this.costsService.getComments();
    this.comments$.subscribe (
      data => {}
    ,
    error =>{
      this.hasError = true;
  }
    )
}
}