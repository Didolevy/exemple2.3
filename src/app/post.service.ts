import { User } from './interfaces/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import {Comment} from './interfaces/comment';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PostService {

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  

  savepostsCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  
  constructor(private http: HttpClient,private db: AngularFirestore) { }

  getPosts():Observable<Post>{

    return this.http.get<Post>(this.URL); 
  }

  savepost(userId:string,id:string,title:string,body:string){
    const post = { title:title,body:body,id:id}; 
    this.userCollection.doc(userId).collection('saveposts').add(post);
  }

}
