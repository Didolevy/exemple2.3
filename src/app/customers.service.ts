import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Customer } from './interfaces/customer';



@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customersCollection:AngularFirestoreCollection; 
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  saveredict(userId:string,id:string,result:string,saved:Boolean){
    console.log(id,result)
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result,
        saved:saved
       
      }
    )
  
  
  }


  updateCustomer(userId:string, id:string,name:string,years:number,income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
        result:null
      }
    )
  }

  addCustomer(userId:string, name:string, years:number, income:number){
    const customer:Customer = {name:name,years:years, income:income}
    this.userCollection.doc(userId).collection('customers').add(customer);
  } 


  public getCustomers(userId): Observable<any[]> {
    this.customersCollection = this.db.collection(`users/${userId}/customers`)
    return this.customersCollection.snapshotChanges();    
  } 

  public deletecustomer(userId:string ,id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
  }


  



  constructor(private db:AngularFirestore, ) { }
}
