import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';

@Component({
  selector: 'welocome',
  templateUrl: './welocome.component.html',
  styleUrls: ['./welocome.component.css']
})
export class WelocomeComponent implements OnInit {
  email:string; 
  user$;

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.user$ = this.authService.getUser(); 
    this.user$.subscribe(
      data => {
        this.email= data.email;
  })

  }
}