import { Savepost } from './../interfaces/savepost';
import { SavepostService } from './../savepost.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-savepost',
  templateUrl: './savepost.component.html',
  styleUrls: ['./savepost.component.css']
})
export class SavepostComponent implements OnInit {
 userId:string;
 saveposts$;
 savepost:Savepost[];
 panelOpenState = false;

  constructor( public authService:AuthService, private savepostService:SavepostService) { }

  public deleteSave(id:string){
    this.savepostService.deleteSave(this.userId,id);
  }

  ngOnInit(): void {
      this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
          this.saveposts$ = this.savepostService.getSave(this.userId);
        }
      )
  
  }

}
