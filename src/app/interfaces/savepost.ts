export interface Savepost {
    userId: number, 
    id: number,
    title: string,
    body: string, 
    saved:Boolean
}
