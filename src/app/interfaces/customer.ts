export interface Customer {
    name: string;
    years: number;
    income: number;
    id?:string;
    saved?:Boolean;
    result?:string;     
    
}
