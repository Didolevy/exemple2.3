import { Customer } from './../interfaces/customer';
import { CustomersService } from './../customers.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { PredtionService } from '../predtion.service';


@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  userId;
  name:any;
  customers:Customer[];
  customers$;
  addCustomerFormOpen;
  editCustomerFormOpen;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {name:null, years:null, income:null};

  
  displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit','Predict','Result','Save'];
 
  
  constructor(private customersService:CustomersService, public authService:AuthService, private predtionService:PredtionService) { }

  save(index){
    console.log(this.userId,this.customers[index].id,this.customers[index].result)
    this.customers[index].saved=true;
    this.customersService.saveredict(this.userId,this.customers[index].id,this.customers[index].result,this.customers[index].saved);
  }

  Search(){
    if(this.name == ""){
      this.ngOnInit();
    }else{
      this.customers = this.customers.filter(
        res =>{
          return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
        }
      )
    }
  }
  prdict(index){
  console.log(this.customers[index].years,this.customers[index].income)
  this.predtionService.predict(this.customers[index].years,this.customers[index].income).subscribe(
    res => {
      console.log(res);
      if(Number(res) > 0.5){
        var result = 'Will pay';
      } else {
        var result = "will not pay";
      }
      this.customers[index].result = result}
  );  
}

  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.name, customer.years, customer.income)
  }
  
   deletecustomer(index){
    let id = this.customers[index].id;
    this.customersService.deletecustomer(this.userId,id);
  }
  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.years = this.customers[index].years;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
  }
  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.years,this.customerToEdit.income);
    this.rowToEdit = null;
  }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true; 
                }
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }                        
            }
          )
      })
  }   }
