import { CommentsService } from './../comments.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../interfaces/post';
import { PostService } from '../post.service';
import { Comment } from '../interfaces/comment';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$;
  post:Post[];
  comments$;
  comments:Comment[];
  title:string;
  body:string;
  userId:string;
  postId:string;
  panelOpenState = false;
  message :"Saved for later viewing";
  items=[];
   
  constructor(private route:ActivatedRoute ,private postsService:PostService, public authService:AuthService ,private CommentsService:CommentsService) { }

  
savepost(id:string,title:string,body:string){
  this.postsService.savepost(this.userId,id,title,body)
  this.items.push(id);
  this.postId = id;
}

  ngOnInit(): void {
    this.posts$=this.postsService.getPosts();
    this.comments$=this.CommentsService.getComments();
    this.posts$.subscribe (
      data => {}
    )
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
   
})
}}