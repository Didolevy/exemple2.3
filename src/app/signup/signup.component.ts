import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email:string;
  password:string;
  errorMessage:string ;
  isError:boolean = false;

  onSubmit(){
    this.auth.signup(this.email,this.password).then(
      res =>{
        console.log('Succesful login;');
        this.router.navigate(['/cards']); 
      }
    ).catch(
      err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 
      } 
    ) 
  }

  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}

