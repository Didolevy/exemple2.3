
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PredtionService {
  private url = "https://loo58na1v8.execute-api.us-east-1.amazonaws.com/gama";

  predict(years:number,income:number){
    let json = {
      "years": years,
      "income": income
    }
    let body = JSON.stringify(json)
    console.log(body);
    return this.http.post<any>(this.url ,body).pipe(
    map(res =>{
      console.log(res);
      let final:string = res.body;  
      console.log(final);
      final = final.replace('[',''); 
      final = final.replace(']','');
      return final; 
    })
    )
  }

  constructor(private http:HttpClient) { }
}
