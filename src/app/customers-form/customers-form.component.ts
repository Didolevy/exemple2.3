import { Component, OnInit ,EventEmitter, Input, Output } from '@angular/core';
import { Customer } from '../interfaces/customer';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'customers-form',
  templateUrl: './customers-form.component.html',
  styleUrls: ['./customers-form.component.css']
})
export class CustomersFormComponent implements OnInit {

  @Input() name: string;
  @Input() years: number;
  @Input() id: string;
  @Input() income: number;
  @Input() formType: string;
  
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  buttonText:String = 'Add customer'; 
  fontSizeControl = new FormControl(12, Validators.max(24));

 
  tellParentToClose(){
    this.closeEdit.emit();
  }
  
  updateParent(){
    let customer:Customer = {id:this.id,name:this.name, years:this.years,income:this.income}; 
    this.update.emit(customer);
    if(this.formType == 'Add customer'){
      this.name = null;
      this.years = null;
      this.income = null; 
    }
  }

  constructor() { }


  ngOnInit(): void {
    if(this.formType == 'Add customer'){
      this.buttonText = 'Add';
    }
  }

}